//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");
//Server Setup
const app = express();
const port = 4000;

//Middleware, para magamit ung user input
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Database Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.pjjaa.mongodb.net/B157_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

//Checking the db connection
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database"));

//Add the task route
//This will allow all the task routes created in the "taskRoute.js" file to use the "/tasks" route
app.use("/tasks", taskRoute);
//http://localhost:4000/tasks/


app.listen(port, () => console.log(`Now Listening to port ${port}`))