//mongoose is used to make a Schema
const mongoose = require("mongoose");

//Creating a New Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: 
		{
			type: String,
			default: "pending"
		}
});

module.exports = mongoose.model("Task", taskSchema)