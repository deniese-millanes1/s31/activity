//Contains all of the endpoints of our application

//We need to use express' Router() function to achieve this

//Insert dependencies
const express = require("express");

//Router() function of express
//Allows access to HTTP method middlewares that makes it easier to create routes for our application 
const router = express.Router();

//Import Controllers
const taskController = require("../controllers/taskController")

//Route to get all the tasks
router.get("/", (req,res) => {
		taskController.getAllTasks().then(resultFromController => 
			res.send(resultFromController));
	});
//".then"(result) 


//Route to create a task
//This route expects to receive POST request at the URL "/tasks/"
//The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
router.post("/", (req,res) => {
	console.log(req.body)
		taskController.createTask(req.body).then(resultFromController => 
			res.send(resultFromController));
	})


//Route for deleting a Task
router.delete("/:id", (req,res) => {
	console.log(req.params.status)
	taskController.deleteTask(req.params.id).then(resultFromController => 
		res.send(resultFromController));
});


//Route for updating a Task
router.put("/:id", (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController =>
		res.send(resultFromController));
});


//Activity
//1.Route Specific Task
router.get("/:id", (req,res) => {
	console.log(req.body)
	taskController.getTask(req.params.id, req.body).then(resultFromController =>
		res.send(resultFromController));
});


//5.Route Changing status>complete
router.put("/:id/complete", (req,res) => {
	taskController.updateStatus(req.params.id).then(resultFromController =>
		res.send(resultFromController));
});



//Use "module.exports" to export the router object to use in the index.js file
module.exports = router;

//the difference between req.params and req.body?