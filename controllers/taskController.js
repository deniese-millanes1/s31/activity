//Controllers contain the functions and business logic of our Express JS application

const Task = require("../models/task");

//Controller function for getting all the tasks
//Defines the function to be used in the "taskRoute.js" file and exports these functions
module.exports.getAllTasks = () => {
			//model.method
	return Task.find({}).then(result => {
		return result;
	})
}

//createTask Controller
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
//Saves the newly created "newTask" object in our MongoDB Database
//The "then" method waits until the task is save or stored in the database or an error is encountered before returning a "true" or "false" value back to the client
//The "then" method will accept 2 arguments
	//1st parameter: will store result returned by the Mongoose "save" method
	//2nd parameter: will store the error object
	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error)
			return false
		} else {
			return task
		}
	})
}


//Controller for deleting a task
//"req.params.id" from taskRoute will become "taskId"
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if (error) {
			console.log(error)
			return false 
		} else {
			return removedTask
		}
	})
}

//Controller for Updating a Task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}
		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) =>{
			if (saveErr){
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})
	})
}


//Activity
//2.Specific Task
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => {
			return result;
		})	
}

//6.Status>Complete
module.exports.updateStatus = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		console.log(result)
		if (error) {
			console.log(error)
			return false 
		} 
		result.status = "complete";
		return result.save().then((changedStatus, saveError) => {
			if (saveError) {
				console.log(saveError)
				return false 
			} else {
				console.log(changedStatus)
				return(changedStatus)
			}
		})
	})
}


// module.exports.createTask = (requestBody) => {
// 	let newTask = new Task({
// 		name: requestBody.name
// 	})

// module.exports.updateTask = (taskId, newContent) => {
// 	return Task.findById(taskId).then((result, error) => {
// 		if (error) {
// 			console.log(error)
// 			return false 
// 		} 
// 		result.name = newContent.name;
// 		return result.save().then((updatedTask, saveError) => {
// 			if (saveError) {
// 				console.log(saveError)
// 				return false 
// 			} else {
// 				console.log(updatedTask)
// 			}
// 		})
// 	})
// }